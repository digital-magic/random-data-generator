package io.digitalmagic

import java.security.SecureRandom
import java.util.UUID

import scala.util.Random

trait Generator[+T] {

  self => // an alias for this()

  def generate: T

  def map[S](f: T => S): Generator[S] = new Generator[S] {
    def generate = f(self.generate)
  }

  def flatMap[S](f: T => Generator[S]): Generator[S] = new Generator[S] {
    override def generate: S = f(self.generate).generate
  }
}

object Generator {
  def generateBoolean(): Boolean = generateInt(1) == 1
  def generateInt(size: Int = Int.MaxValue): Int = new IntGenerator(size).generate
  def generateString(length: Int, chars: String): String = new StringGenerator(length, chars).generate
  def generateNumericString(length: Int = 10): String = new NumericStringGenerator(length).generate
  def generateAlphaString(length: Int = 10): String = new AlphaStringGenerator(10).generate
  def generateAlphanumericString(length: Int = 10): String = new AlphanumericStringGenerator(length).generate
  def generateBearerAccessToken(length: Int = 255): String = new BearerAccessTokenGenerator(length).generate
  def generateUUID(): String = new UUIDStringGenerator().generate

  implicit class CollectionGenerator[T](collection: Iterable[T]) {
    val intGenerator = new IntGenerator(collection.size)
    def generator(): Generator[T] = for { index <- intGenerator } yield collection.iterator.drop(index).next()
    def randomElement(): T = generator().generate
    def randomElements(n: Int): Iterable[T] = for { x <- 0 until n } yield randomElement()
  }
}

class IntGenerator(size: Int = Int.MaxValue) extends Generator[Int] {
  var rand = new Random(System.nanoTime)
  override def generate: Int = rand.nextInt(size)
}

class UUIDStringGenerator extends Generator[String] {
  override def generate: String = UUID.randomUUID().toString
}

class StringGenerator(length: Int, chars: String) extends Generator[String] {

  private val random = new SecureRandom()

  protected def generate(len: Int): String = {
    if (len == 0) ""
    else chars(random.nextInt(chars.length)) + generate(len - 1)
  }

  override def generate: String = generate(length)
}

class NumericStringGenerator(length: Int) extends StringGenerator(length, "0123456789")
class AlphaStringGenerator(length: Int) extends StringGenerator(length, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz")
class AlphanumericStringGenerator(length: Int) extends StringGenerator(length, "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz")
class BearerAccessTokenGenerator(length: Int) extends StringGenerator(length, "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-._")
