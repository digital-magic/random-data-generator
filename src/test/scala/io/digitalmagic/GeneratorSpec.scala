package io.digitalmagic

import org.scalatest.{Matchers, WordSpec}
import Generator.CollectionGenerator

class GeneratorSpec extends WordSpec with Matchers {

  private val testCollection = (1 to 10).toList

  "Generator" must {
    "generate correct strings" in {
      // TODO: Add checks for size and generated string characters
      Generator.generateAlphaString() shouldBe a [String]
      Generator.generateAlphanumericString() shouldBe a [String]
      Generator.generateNumericString() shouldBe a [String]
      Generator.generateBearerAccessToken() shouldBe a [String]
    }

    "generate correct boolean" in {
      Generator.generateBoolean() shouldBe a [java.lang.Boolean] // TODO: Find out how to avoid autoboxing here
    }

    "generate correct number" in {
      Generator.generateInt() shouldBe a [java.lang.Integer] // TODO: Find out how to avoid autoboxing here
    }

    "generate correct UUID" in {
      Generator.generateUUID() shouldBe a [String] // TODO: Additional checks required that UUID is correct
    }

    "enrich collection and be able to generate correct random number for it's size" in {
      for (i <- 0 to 100) {
        testCollection.intGenerator.generate should (be >= 0 and be < testCollection.size)
      }
    }

    "select random element for enriched collection" in {
      testCollection should contain (testCollection.randomElement())
    }

    "select random sub-collection for enriched collection" in {
      val selectCount = 3
      val subCollection = testCollection.randomElements(selectCount)
      subCollection should have size selectCount
      for (el <- subCollection) {
        testCollection should contain(el)
      }
    }
  }

}
