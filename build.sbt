name := "data-generator"

organization := "io.digital-magic"

version := "0.1"

scalaVersion := "2.12.3"

resolvers += Resolver.mavenLocal

libraryDependencies ++= {
  val scalaTestVersion  = "3.0.1"
  Seq(
    "org.scalatest"     %% "scalatest" % scalaTestVersion % "test"
  )

}
